<?php
/**
 * Widget Content-List Template: Single Play Game Content List Button
 */

$post_id = get_the_ID();

if (isset($data->query) && is_object($data->query) && $data->query->have_posts()): ?>

    <div class="contentList--variant1">

        <h2><?php echo get_the_title($post_id); ?></h2>

        <ul class="contentList--variant1__list">

            <?php while ($data->query->have_posts()):$data->query->the_post(); ?>

                <?php if (isset($data->item) && $data->item) : ?>

                    <li class="contentList--variant1__item">
                        <?php
                        $slug = 'play';
                        $link = "/{$slug}?id={$post_id}&game=" . get_post_meta($post_id, 'game_game', true);

                        $link = str_replace('wp-game.', 'onnet-', $link);
                        ?>
                        <a class="contentList--variant1__innerWrapper" href="<?php echo $link; ?>">

                            <?php if (isset($data->item->thumbnail) && $data->item->thumbnail != "no_thumb") : ?>

                                <?php if (isset($data->item->thumbnail) && $data->item->thumbnail == "post_thumb") : ?>

                                    <span
                                        class="contentList--variant1__imgWrapper"><?php new OnNet_image($data->item->image_size); ?></span>

                                <?php elseif (isset($data->item->thumbnail) && $data->item->thumbnail == "tt_thumb") : ?>

                                    <?php if (get_the_term_thumbnail() !== null) : ?>

                                        <span class="contentList--variant1__imgWrapper"><img
                                                src="<?php echo get_the_term_thumbnail($data->item->image_size); ?>"/></span>

                                    <?php endif; ?>

                                <?php endif; ?>
                            <?php else: ?>

                                <span
                                    class="contentList--variant1__imgWrapper"><?php new OnNet_image('thumbnail', get_post_thumbnail_id($post_id)); ?></span>

                            <?php endif; ?>

                            <div class="contentList--variant1__textWrapper">

                                <?php
                                $description = get_club_description($slug);

                                echo '<h3>';
                                if($description)
                                    echo $description;
                                else
                                    echo get_the_content();
                                echo '</h3>';
                                ?>
                            </div>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endwhile; ?>
        </ul>

    </div>
    <div class="ctaUnit--variant1">
        <div class="ctaUnit--variant1__body">
            <div>
                <a href="<?php echo $link; ?>">Play Game</a>
            </div>
        </div>
    </div>
<?php endif;