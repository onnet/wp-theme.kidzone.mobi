<?php
/**
 * Widget Interstial Template: Content-List Item [Variant 1]
 */


if (empty($data->interstitials_cta_link))
    return;

if (empty($data->interstitials_body))
    return;

?>
<li class="contentList--variant1__item">
    <a class="contentList--variant1__innerWrapper" href="<?php echo $data->interstitials_cta_link; ?>">
        <div class="contentList--variant1__textWrapper">

            <?php
                    echo $data->interstitials_body;
            ?>
        </div>

        <?php if (!is_feature_phone() && !empty($data->interstitials_image)): ?>
            <span class="contentList--variant1__imgWrapper">
               <?php new OnNet_image('thumbnail', $data->interstitials_image); ?>
            </span>
        <?php endif; ?>
    </a>
</li>