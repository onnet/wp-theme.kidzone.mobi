<?php
/**
 * Widget Multi Digit Template: Content-List Wrapper [Variant 1]
 */
?>
<div class="contentList--variant1">

    <?php
    // Page Title
    if (!empty($title))
        echo "<h2>{$title}</h2>";
    else
        echo "<h2>" .get_the_title() . "</h2>";
    ?>

    <ul class="contentList--variant1__list">
        <?php echo $the_digit; ?>
    </ul>

</div>